# lisprs

My attempt to write a lisp interpreter using Rust.

Inspired by Peter Norvig's [(How to Write a (Lisp) Interpreter (in Python))](http://norvig.com/lispy.html)
