use std::collections::HashMap;
use std::f64::consts;

#[derive(Debug, PartialEq, Clone)]
pub enum Expr {
    List(Vec<Expr>),
    Number(f64), // Making our only number floating, could look at having int type as well?
    Symbol(String),
    Noop,
}

#[derive(Debug, PartialEq)]
pub enum SyntaxError {
    Unexpected(String),
}

// TODO: Seperate file for Environment?
struct Environment {
    vars: HashMap<String, Expr>,
}

impl Environment {
    fn new() -> Self {
        Self {
            vars: HashMap::new(),
        }
    }

    // TODO: Should this return a Result for user error handling?
    fn access(&self, var: &String) -> Expr {
        match self.vars.get(var) {
            Some(x) => x.clone(),
            None => panic!("Can't access undefined variable '{}'", var),
        }
    }

    // TODO: Should this return a Result for user error handling?
    fn define(&mut self, var: &String, val: Expr) {
        match self.vars.insert(var.to_owned(), val) {
            Some(_) => panic!("Cannot define variable '{}' as it is alrady defined!", var),
            None => (),
        }
    }
}

pub struct Lisp {
    env: Environment,
}

impl Lisp {
    pub fn new() -> Self {
        // Setup env.
        let mut env = Environment::new();
        env.define(&("pi".to_string()), Expr::Number(consts::PI));

        Self {
            env,
        }
    }

    pub fn run(&mut self, src: &str) {
        match self.parse(src) {
            Ok(expr) => {
                let result = self.eval(expr);
                println!("=> {:?}", result)
            },
            Err(err) => eprintln!("{:?}", err),
        }
    }

    pub fn parse(&self, src: &str) -> Result<Expr, SyntaxError> {
        let mut tokens = self.tokenize(src);
        self.read_from_tokens(&mut tokens)
    }

    fn tokenize(&self, src: &str) -> Vec<String> {
        src.replace("(", " ( ")
            .replace(")", " ) ")
            .split(" ")
            .map(|s| s.to_string())
            .filter(|s| s != "")
            .collect()
    }

    fn read_from_tokens(&self, tokens: &mut Vec<String>) -> Result<Expr, SyntaxError> {
        if tokens.len() == 0 {
            return Err(SyntaxError::Unexpected("EOF".to_string()));
        }

        let token = tokens.remove(0);

        if token == "(" {
            let mut expr_list = Vec::new();
            while tokens[0] != ")" {
                let expr = self.read_from_tokens(tokens)?;
                expr_list.push(expr);
            }
            tokens.remove(0);
            return Ok(Expr::List(expr_list));
        } else if token == ")" {
            return Err(SyntaxError::Unexpected(")".to_string()));
        } else {
            match token.parse::<f64>() {
                Ok(num) => return Ok(Expr::Number(num)),
                Err(_) => return Ok(Expr::Symbol(token)),
            }
        }
    }

    fn eval(&mut self, expr: Expr) -> Expr {
        match expr {
            Expr::Symbol(x) => self.env.access(&x),
            Expr::Number(_) => expr,
            Expr::List(list) => {
                let mut arguments = list;
                let procedure_name = arguments.remove(0);

                match procedure_name {
                    Expr::Symbol(symbol) => {
                        match symbol.as_str() {
                            "define" => {
                                let var = arguments.remove(0);
                                let exp = arguments.remove(0);
                                // Make sure variable name is symbol
                                let var_name = match var {
                                    Expr::Symbol(x) => x,
                                    _ => panic!("Variable name must be symbol!"),
                                };
                                let evaluated = self.eval(exp);
                                self.env.define(&var_name, evaluated);

                                Expr::Noop // does this make sense?
                            },
                            "quote" => arguments.remove(0),
                            "if" => unimplemented!(),
                            _ => {
                                // defined procedure call
                                let evaluated_arguments: Vec<Expr> = arguments.iter()
                                    .map(|a| self.eval(a.clone()))
                                    .collect();
                                self.call_procedure(&symbol, evaluated_arguments)
                            },
                        }
                    }
                    _ => unreachable!(), // I seem to get here on syntax errors like (+2 2) instead of (+ 2 2)
                }
            }
            _ => unreachable!(),
        }
    }

    fn call_procedure(&self, name: &str, mut arguments: Vec<Expr>) -> Expr {
        match name {
            "+" => self.do_arithmatic(arguments, add),
            "-" => self.do_arithmatic(arguments, subtract),
            "*" => self.do_arithmatic(arguments, multiply),
            "/" => self.do_arithmatic(arguments, divide),
            "begin" => {
                match arguments.pop() {
                    Some(x) => x,
                    None => Expr::Noop, // TODO: REVISIT
                }
            }
            _ => panic!("unknown procedure - wat"),
        }
    }

    fn do_arithmatic<F: Fn(f64, f64) -> f64>(&self, arguments: Vec<Expr>, operation: F) -> Expr {
        let mut accumulator = 0f64;

        // TODO: Can this be turned into a fold?
        for (i, arg) in arguments.iter().enumerate() {
            accumulator = match arg {
                Expr::Number(operand) => {
                    if i == 0 {
                        *operand
                    } else {
                        operation(accumulator, *operand)
                    }
                },
                _ => panic!("Operands of arithmatic must be numbers."),
            }
        }

        Expr::Number(accumulator)
    }
}

// Math helpers for evaluation
 fn add(a: f64, b: f64) -> f64 {
    a + b
}

fn subtract(a: f64, b: f64) -> f64 {
    a - b
}

fn multiply(a: f64, b: f64) -> f64 {
    a * b
}

fn divide(a: f64, b: f64) -> f64 {
    a / b
}

#[cfg(test)]
mod tests {

    //TODO: Addition of tests for odd syntax like "())" or "((" to alert user of correct issue
    // either unexpected input or EOF.
    use super::*;

    #[test]
    fn tokenize_empty_string() {
        let lisp = Lisp::new();
        let expected: Vec<String> = Vec::new();

        assert_eq!(expected, lisp.tokenize(""))
    }

    #[test]
    fn tokenize_valid_input() {
        let lisp = Lisp::new();
        let expected: Vec<String> = vec![
            "(", "begin", "(", "define", "r", "10", ")", "(", "*", "pi", "(", "*", "r", "r", ")",
            ")", ")",
        ]
        .iter()
        .map(|s| s.to_string())
        .collect();

        assert_eq!(
            expected,
            lisp.tokenize("(begin (define r 10) (* pi (* r r)))")
        )
    }

    #[test]
    fn read_from_tokens_empty() {
        let lisp = Lisp::new();
        let expected = Err(SyntaxError::Unexpected("EOF".to_string()));
        let mut tokens = lisp.tokenize("");

        assert_eq!(expected, lisp.read_from_tokens(&mut tokens))
    }

    #[test]
    fn read_from_tokens_unexpected_paren() {
        let lisp = Lisp::new();
        let mut tokens = lisp.tokenize(")");
        let expected = Err(SyntaxError::Unexpected(")".to_string()));

        assert_eq!(expected, lisp.read_from_tokens(&mut tokens))
    }

    #[test]
    fn read_from_tokens_simple_expr() {
        let lisp = Lisp::new();
        let mut tokens = lisp.tokenize("(* 2 2)");
        let expected = Ok(Expr::List(vec![
            Expr::Symbol("*".to_string()),
            Expr::Number(2.0),
            Expr::Number(2.0),
        ]));

        assert_eq!(expected, lisp.read_from_tokens(&mut tokens))
    }

    #[test]
    fn read_from_tokens_longer_expr() {
        let lisp = Lisp::new();
        let mut tokens = lisp.tokenize("(begin (define r 10) (* pi (* r r)))");
        let expected = Ok(Expr::List(vec![
            Expr::Symbol("begin".to_string()),
            Expr::List(vec![
                Expr::Symbol("define".to_string()),
                Expr::Symbol("r".to_string()),
                Expr::Number(10.0),
            ]),
            Expr::List(vec![
                Expr::Symbol("*".to_string()),
                Expr::Symbol("pi".to_string()),
                Expr::List(vec![
                    Expr::Symbol("*".to_string()),
                    Expr::Symbol("r".to_string()),
                    Expr::Symbol("r".to_string()),
                ]),
            ]),
        ]));

        assert_eq!(expected, lisp.read_from_tokens(&mut tokens))
    }
}
