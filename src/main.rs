mod lisp;

use crate::lisp::Lisp;
use std::io::{stdin, stdout, Write};

fn main() {
    let mut lisp = Lisp::new();

    // Basic REPL setup.  We give a message of usage, then repeatedly allow enter delimited input.

    println!("Welcome to the lisprs repl");
    println!("Type your lisp expression(s) to evaluate.\n");

    loop {
        let mut input = String::new();

        print!("lisprs> ");
        stdout().flush().unwrap();

        // TODO: This doesnt seem the cleanest way to write. Maybe just call unwrap
        // and have main return result?
        match stdin().read_line(&mut input) {
            Ok(_) => (),
            Err(e) => eprintln!("Error occurred: {}", e),
        };

        lisp.run(&input.trim());
    }
}
